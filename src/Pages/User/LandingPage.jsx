import Header from "../../Components/User/Header";
import Footer from "../../Components/User/Footer";
import OurServices from "../../Components/User/OurServices";
import WhyUs from "../../Components/User/WhyUs";
import Testimonial from "../../Components/User/Testimonial";
import CtaBanner from "../../Components/User/CtaBanner";
import Faq from "../../Components/User/Faq";
import { useEffect } from "react";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import "./LandingPage.css";

const LandingPage = () => {
  const navigate = useNavigate();
  const data = useSelector((globalStore) => globalStore);

  const CheckLogin = () => {
    if (data.auth.dataLogin === null ||
      data.auth.dataLogin?.email === "admin@admin.com") 
      navigate("/");
  };

  useEffect(() => {
    CheckLogin();
    document.body.style.background = "white";
    // eslint-disable-next-line
  }, []);

  return (
    <>
      <Header />
      <OurServices />
      <WhyUs />
      <Testimonial />
      <CtaBanner />
      <Faq />
      <Footer />
    </>
  );
};

export default LandingPage;
